# Ebay-Simulation

## General info
The main goal of project is to Emulator websites that are for buying and selling second-hand appliances.

## How it works
It's a multi Server-Client application with Java and JavaFX library, so you can connect with [Ebay-Server](https://gitlab.com/mahsapeymancs/ebayserver) project and users information will be saved every
time they log in. The information will be sent and received with JsonObjects.

## Capabilities
* Add your advertisement
* Chat with the seller
* Bookmark any item
* Advanced search

## Technologies
* JavaFX
* JDK 1.8
* Maven
* MySQL
* CSS

This is the first look of the app you are going to use:

![Allure repeat](https://i.postimg.cc/J4p3gLwx/Screenshot-640.png)