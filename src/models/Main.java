package models;

import controllers.PagesController;
import javafx.application.Application;
import javafx.stage.Stage;
import models.client.Advertisement;
import models.client.User;

public class Main extends Application
{
    public static Advertisement currentAdvertisement;
    public static User currentUser = new User();
    public static final String CURRENCY = "$";
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        PagesController.openPage("login");
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}