package models.client;

import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;
import controllers.PagesController;
import javafx.application.Platform;
import javafx.scene.input.MouseEvent;

import java.io.IOException;

public class Notification
{
    String title;
    String message;
    Notifications notification;
    public Notification(String title, String message, Notifications notification)
    {
        this.title = title;
        this.message = message;
        this.notification = notification;

        TrayNotification tray = new TrayNotification();
        tray.setTitle(this.title);
        tray.setMessage(this.message);
        tray.setNotification(this.notification);
        tray.showAndWait();
    }

    public void mousePressed(MouseEvent e)
    {
        if(e.getClickCount() >= 1)
        {
            Platform.runLater(() ->
            {
                PagesController.closePage();
                try
                {
                    PagesController.openPage("chatWithCustomer");
                }
                catch (IOException ex)
                {
                    throw new RuntimeException(ex);
                }
            });
        }
    }
}
