package models.client;
import java.awt.*;
import java.util.UUID;

public class User
{
    private static String password;
    private static String phoneNumber;
    private static String emailAddress;
    private static String id;
    private static String firstName;
    private static String lastName;
    private static String city;
    byte[] profileImage;

    public void setId(String id)
    {
        this.id = id;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public static String getId()
    {
        return id;
    }

    public static String getCity()
    {
        return city;
    }

    public static String getFirstName()
    {
        return firstName;
    }

    public static String getLastName()
    {
        return lastName;
    }

    public static String getPassword()
    {
        return password;
    }

    public static String getEmailAddress()
    {
        return emailAddress;
    }

    public static String getPhoneNumber()
    {
        return phoneNumber;
    }

    public byte[] getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(byte[] profileImage) {
        this.profileImage = profileImage;
    }
}