package models.client;

import org.json.JSONObject;

public class ClientTask
{
    public static String signUp = "{\"task\":\"signup\",\"name\":\"%s\",\"city\":\"%s\",\"password\":\"%s\"" +
            ",\"phoneNumber\":\"%s\",\"emailAddress\":\"%s\"}";
    public static String login = "{\"task\":\"login\",\"entranceCode\":\"%s\",\"password\":\"%s\"}";
    public static String addNewAd = "{\"task\":\"addNewAd\",\"name\":\"%s\",\"brand\":\"%s\",\"category\":\"%s\",\"city\":\"%s\",\"price\":\"%s\"," +
            "\"description\":\"%s\"}";
    public static String response = "{\"type\":\"response\",\"state\":%b,\"message\":\"%s\"}";
    public static String editFirstName = "{\"task\":\"editFirstName\",\"firstName\":\"%s\",\"username\":\"%s\"}";
    public static String editLastName = "{\"task\":\"editLastName\",\"lastName\":\"%s\",\"username\":\"%s\"}";
    public static String editPhoneNumber = "{\"task\":\"editPhoneNumber\",\"phoneNumber\":\"%s\",\"username\":\"%s\"}";
    public static String editEmailAddress = "{\"task\":\"editEmailAddress\",\"emailAddress\":\"%s\",\"username\":\"%s\"}";
    public static String editCity = "{\"task\":\"editCity\",\"city\":\"%s\",\"username\":\"%s\"}";
    public static String editPassword = "{\"task\":\"editPassword\",\"password\":\"%s\",\"username\":\"%s\"}";
    // From sender to server
    public static String chat = "{\"task\":\"chat\",\"senderId\":\"%s\",\"recipientId\":\"%s\",\"message\":\"%s\"}";
    public static String exit = "{\"task\":\"exit\",\"username\":\"%s\"}";
    // From server to recipient
    public static String sendChatInfo = "{\"type\":\"receivedChat\",\"message\":\"%s\",\"senderId\":\"%s\",\"recipientId\":\"%s\"}";
    public static String homeAds = "{\"username\":\"%s\",\"category\":\"%s\",\"city\":\"%s\",\"name\":\"%s\",\"brand\":\"%s\",\"price\":\"%s\"" +
            ",\"description\":\"%s\"}";
    public static String handleHomeAds = "{\"type\":\"homeAds\",\"homeAds\":\"%s\"}";
    public static String getAddNewAd = "{\"Task\":\"addNewAd\",\"name\":\"%s\",\"category\":\"%s\",\"brand\":\"%s\",\"city\":\"%s\"" +
            ",\"username\":\"%s\",\"description\":\"%s\",\"price\":\"%s\"}";
    public static String loadBookmarks = "{\"type\":\"loadBookmarks\",\"message\":\"%s\"}";
    public static String loadSearchResult = "{\"type\":\"loadSearchResult\",\"message\":\"%s\"}";
    public static String extractSearchResult = "{\"task\":\"search\",\"group\":\"%s\",\"keyWord\":\"%s\"}";
    public static String bookmark = "{\"task\":\"bookmark\",\"senderUsername\":\"%s\",\"name\":\"%s\",\"category\":\"%s\",\"brand\":\"%s\",\"city\":\"%s\"" +
            ",\"username\":\"%s\",\"description\":\"%s\",\"price\":\"%s\"}";
    public static String extractBookmarks = "{\"task\":\"extractBookmarks\",\"senderUsername\":\"%s\"}";
    public static String advisorInfo = "{\"firstName\":\"%s\",\"lastName\":\"%s\",\"city\":\"%s\"" +
            ",\"phoneNumber\":\"%s\",\"emailAddress\":\"%s\",\"username\":\"%s\"}";
    public static String extractAdvisorInfo = "{\"task\":\"extractAdvisorInfo\",\"username\":\"%s\"}";
    public static String loadAdvisorInfo = "{\"type\":\"loadAdvisorInfo\",\"message\":\"%s\"}";
    public static String loadProfile = "{\"task\":\"extractProfile\",\"username\":\"%s\"}";
    public static String deleteBookmark = "{\"task\":\"deleteBookmark\",\"senderUsername\":\"%s\",\"name\":\"%s\",\"category\":\"%s\",\"brand\":\"%s\",\"city\":\"%s\"" +
            ",\"username\":\"%s\",\"description\":\"%s\",\"price\":\"%s\"}";



}