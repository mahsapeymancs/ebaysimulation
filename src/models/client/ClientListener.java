package models.client;

import controllers.LoginController;
import controllers.MainPageController;
import controllers.PagesController;
import javafx.application.Platform;
import models.Main;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class ClientListener extends ServerMessageHandling  implements Runnable
{
    public static ArrayList<Advertisement> homeAdvertisements = new ArrayList<>();
    public static ArrayList<Advertisement> bookmarkedAds = new ArrayList<>();
    public static ArrayList<Advertisement> searchedAds = new ArrayList<>();
    public static ArrayList<JSONObject> previousChats = new ArrayList<>();
    public static User advisor = new User();
    public static User user = new User();
    public static String serverMessage;
    public static boolean serverState;

    public static String textMessage;
    private InputStream in;
    private Thread thread;

    public ClientListener(InputStream in)
    {
        this.in = in;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run()
    {
        byte[] buffer = new byte[2000];
        while (true)
        {
            int len = 0;
            try
            {
                len = in.read(buffer);
                byte[] tmp = new byte[len];
                System.arraycopy(buffer, 0, tmp, 0, len);
                String message = new String(tmp);
                System.out.println("Server message");
                System.out.println(message);
                System.out.println(new JSONObject(message).getString("type"));
                handleServerMessages(message);
                System.out.println(message);
                buffer = new byte[2000];
            }

            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void handleServerMessages(String message)
    {

        JSONObject data = new JSONObject(message);


        String type = data.getString("type");

        if (type.equalsIgnoreCase("receivedChat"))
        {
            textMessage = receiveMessage(data);
        }
        else if (type.equalsIgnoreCase("response"))
        {
            serverMessage = getServerMessage(data);
            serverState = getServerState(data);
            homeAdvertisements = homeAds(data);
//            passData(homeAds(data), getServerMessage(data), getServerState(data));

            if (serverState)
            {
                Platform.runLater(() ->
                {
                    PagesController.closePage();
                    try
                    {
                        PagesController.openPage("mainPage");
                    }
                    catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
                });

            }
        }
        else if (type.equalsIgnoreCase("loadBookmarks"))
        {
            bookmarkedAds = loadBookmarks(data);
        }
        else if (type.equalsIgnoreCase("loadSearchResult"))
        {
            homeAdvertisements = loadSearchResult(data);
            if(true)
            {
                Platform.runLater(() ->
                {
                    PagesController.closePage();
                    try
                    {
                        PagesController.openPage("mainPage");
                    }
                    catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
                });
            }
        }
        else if (type.equalsIgnoreCase("loadChats"))
        {
            previousChats = loadChats(data);
//             This JSONObject arraylist returns all chats with keys : senderUsername and text.
        }
        else if (type.equalsIgnoreCase("loadAdvisorInfo"))
        {
            advisor = loadAdvisorInfo(data);
        }
        else if (type.equalsIgnoreCase("loadProfile"))
        {
            user = loadProfileInfo(data);
            Main.currentUser = user;
        }

    }

    public static boolean isServerState() {
        return serverState;
    }
}
