package models.client;

import controllers.ChatController;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

public class ServerMessageHandling
{

    static public ArrayList<Advertisement>  homeAds(JSONObject data)
    {
        ArrayList<Advertisement> homeFeedAds = new ArrayList<>();
        ArrayList<String> adInfo = new ArrayList<>();
        String adsData = data.getString("homeAds");
        int index = adsData.indexOf("|");
        while (index != -1)
        {
            String str = adsData.substring(0, index);
            adInfo.add(str);
            if (index != adsData.length() - 1)
            {
                adsData = adsData.substring(index + 1);
                index = adsData.indexOf("|");
            }
            else {
                index = -1;
            }
        }
        for (int i = 0; i < adInfo.size(); i++)
        {
            JSONObject ad = new JSONObject(adInfo.get(i));
            Advertisement advertisement = new Advertisement();
            advertisement.setBrand(ad.getString("brand"));
            advertisement.setCategory(ad.getString("category"));
            advertisement.setCity(ad.getString("city"));
            advertisement.setDescription(ad.getString("description"));
            advertisement.setName(ad.getString("name"));
            advertisement.setPrice(ad.getString("price"));
            advertisement.setUsername(ad.getString("username"));
            advertisement.setBookmark(ad.getString("bookmark"));
            homeFeedAds.add(advertisement);
        }
        return homeFeedAds;
    }
    static public String receiveMessage(JSONObject data)
    {
        String senderUsername = data.getString("senderId");
        String textMessage = data.getString("message");
        return textMessage;
    }

    static public String getServerMessage(JSONObject data)
    {
        boolean state = data.getBoolean("state");
        /*
        If state is true serverMessage should be written in green color,
         and if it's false it should be written in red color.

         */
        String severeMessage = data.getString("message");
        return severeMessage;
    }

    static public boolean getServerState(JSONObject data)
    {
        boolean state = data.getBoolean("state");
        /*
        If state is true serverMessage should be written in green color,
         and if it's false it should be written in red color.

         */
        String severeMessage = data.getString("message");
        return state;
    }

    static public ArrayList<Advertisement> loadBookmarks(JSONObject data)
    {
        ArrayList<Advertisement> bookmarks = new ArrayList<>();
        ArrayList<String> adInfo = new ArrayList<>();
        String adsData = data.getString("message");
        int index = adsData.indexOf("|");
        while (index != -1)
        {
            String str = adsData.substring(0, index);
            adInfo.add(str);
            if (index != adsData.length() - 1)
            {
                adsData = adsData.substring(index + 1);
                index = adsData.indexOf("|");
            }
            else
            {
                index = -1;
            }
        }
        for (int i = 0; i < adInfo.size(); i++)
        {
            JSONObject ad = new JSONObject(adInfo.get(i));
            Advertisement advertisement = new Advertisement();
            advertisement.setBrand(ad.getString("brand"));
            advertisement.setCategory(ad.getString("category"));
            advertisement.setCity(ad.getString("city"));
            advertisement.setDescription(ad.getString("description"));
            advertisement.setName(ad.getString("name"));
            advertisement.setPrice(ad.getString("price"));
            advertisement.setUsername(ad.getString("username"));
            bookmarks.add(advertisement);
        }
        return bookmarks;
    }
    static public ArrayList<Advertisement> loadSearchResult(JSONObject data)
    {
        ArrayList<Advertisement> searchResult = new ArrayList<>();
        ArrayList<String> adInfo = new ArrayList<>();
        String adsData = data.getString("message");
        int index = adsData.indexOf("|");
        while (index != -1)
        {
            String str = adsData.substring(0, index);
            adInfo.add(str);
            if (index != adsData.length() - 1)
            {
                adsData = adsData.substring(index + 1);
                index = adsData.indexOf("|");
            }
            else
            {
                index = -1;
            }
        }
        for (int i = 0; i < adInfo.size(); i++)
        {
            JSONObject ad = new JSONObject(adInfo.get(i));
            Advertisement advertisement = new Advertisement();
            advertisement.setBrand(ad.getString("brand"));
            advertisement.setCategory(ad.getString("category"));
            advertisement.setCity(ad.getString("city"));
            advertisement.setDescription(ad.getString("description"));
            advertisement.setName(ad.getString("name"));
            advertisement.setPrice(ad.getString("price"));
            advertisement.setUsername(ad.getString("username"));
            searchResult.add(advertisement);
        }
        return searchResult;
    }
    static public ArrayList<JSONObject> loadChats(JSONObject data)
    {
        ArrayList<JSONObject> previousChats = new ArrayList<>();
        String chats = data.getString("message");
        ArrayList<String> chatInfo = new ArrayList<>();
        int index = chats.indexOf("|");
        while (index != -1)
        {
            String str = chats.substring(0, index);
            if (index != chats.length() - 1)
            {
                chats = chats.substring(index + 1);
            }
            chatInfo.add(str);
            index = chats.indexOf("|");
        }
        for (int i = 0; i < chatInfo.size(); i++)
        {
            JSONObject chat = new JSONObject(chatInfo.get(i));
            previousChats.add(chat);
        }
        return previousChats;
    }

    static public User loadAdvisorInfo(JSONObject data)
    {
        User advisor = new User();
        JSONObject info = new JSONObject(data.getString("message"));
        advisor.setFirstName(info.getString("firstName"));
        advisor.setLastName(info.getString("lastName"));
        advisor.setEmailAddress(info.getString("emailAddress"));
        advisor.setPhoneNumber(info.getString("phoneNumber"));
        advisor.setId(info.getString("username"));
        advisor.setCity(info.getString("city"));
        return advisor;
    }

    static public User loadProfileInfo(JSONObject data)
    {
        User currentUser = new User();
        JSONObject info = new JSONObject(data.getString("message"));
        currentUser.setFirstName(info.getString("firstName"));
        currentUser.setLastName(info.getString("lastName"));
        currentUser.setEmailAddress(info.getString("emailAddress"));
        currentUser.setPhoneNumber(info.getString("phoneNumber"));
        currentUser.setId(info.getString("username"));
        currentUser.setCity(info.getString("city"));
        return currentUser;
    }
}
