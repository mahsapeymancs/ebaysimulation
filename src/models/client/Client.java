package models.client;

import models.Main;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;


public class Client
{

    private static Socket socket;
    private static OutputStream out;
    private static ClientListener cl;
    public static void login(String entranceCode, String password) throws IOException
    {
        out = new OutputStream()
        {
            @Override
            public void write(int b) throws IOException
            {

            }
        };
        try
        {
            socket = new Socket("127.0.0.1", 8000);
            cl= new ClientListener(socket.getInputStream());
            out = socket.getOutputStream();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        JSONObject clientData = new JSONObject(ClientTask.login);

        byte[] buffer1 = entranceCode.getBytes();

        String str1 = new String(buffer1);
        clientData.remove("entranceCode");
        clientData.put("entranceCode", str1);

        byte[] buffer2 = password.getBytes();
        String str2 = new String(buffer2);
        clientData.remove("password");
        clientData.put("password", str2);

        String str = clientData.toString();
        byte[] buffer = str.getBytes(StandardCharsets.UTF_8);
        out.write(buffer);
        out.flush();
    }


    public static void signup(String firstName, String lastName, String city, String email,
                              String phone, String password, byte[] photo) throws IOException
    {
        out = new OutputStream()
        {
            @Override
            public void write(int b) throws IOException
            {

            }
        };
        try
        {
            socket = new Socket("127.0.0.1", 8000);
            cl= new ClientListener(socket.getInputStream());
            out = socket.getOutputStream();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        JSONObject clientData = new JSONObject(ClientTask.signUp);

        byte[] buffer1 = firstName.getBytes();
        String str1 = new String(buffer1);
        clientData.remove("firstName");
        clientData.put("firstName", str1);

        byte[] buffer2 = lastName.getBytes();
        String str2 = new String(buffer2);
        clientData.remove("lastName");
        clientData.put("lastName", str2);

        byte[] buffer3 = city.getBytes();
        String str3 = new String(buffer3);
        clientData.remove("city");
        clientData.put("city", str3);

        byte[] buffer4 = password.getBytes();
        String str4 = new String(buffer4);
        clientData.remove("password");
        clientData.put("password", str4);

        byte[] buffer5 = phone.getBytes();
        String str5 = new String(buffer5);
        clientData.remove("phoneNumber");
        clientData.put("phoneNumber", str5);

        byte[] buffer6 = email.getBytes();
        String str6 = new String(buffer6);
        clientData.remove("emailAddress");
        clientData.put("emailAddress", str6);

        String str = clientData.toString();
        byte[] buffer = str.getBytes(StandardCharsets.UTF_8);
        out.write(buffer);
        out.flush();

        FileOutputStream file = new FileOutputStream("D:\\ProfileImages" + "/" + email + ".jpg");
        file.write(photo, 0, photo.length);

    }


    public static void addNewAds(String name, String city, String category, String brand,
                                 String price, String description, byte[] photo) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        JSONObject clientData = new JSONObject(ClientTask.addNewAd);

        byte[] buffer1 = name.getBytes();
        String str1 = new String(buffer1);
        clientData.remove("name");
        clientData.put("name", str1);

        byte[] buffer2 = city.getBytes();
        String str2 = new String(buffer2);
        clientData.remove("city");
        clientData.put("city", str2);

        byte[] buffer3 = category.getBytes();
        String str3 = new String(buffer3);
        clientData.remove("category");
        clientData.put("category", str3);

        byte[] buffer4 = brand.getBytes();
        String str4 = new String(buffer4);
        clientData.remove("brand");
        clientData.put("brand", str4);

        byte[] buffer5 = price.getBytes();
        String str5 = new String(buffer5);
        clientData.remove("price");
        clientData.put("price", str5);

        byte[] buffer6 = description.getBytes();
        String str6 = new String(buffer6);
        clientData.remove("description");
        clientData.put("description", str6);
        String str = clientData.toString();

        byte[] buffer = str.getBytes(StandardCharsets.UTF_8);
        out.write(buffer);
        out.flush();

        FileOutputStream file = new FileOutputStream("D:\\AdsImages" + "/" + name + ".jpg");
        file.write(photo, 0, photo.length);
    }


    public static void editFirstName(String firstName) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
            JSONObject clientData = new JSONObject(ClientTask.editFirstName);

            byte[] buffer1 = firstName.getBytes();
            String str1 = new String(buffer1);
            clientData.remove("firstName");
            clientData.put("firstName", str1);

            String str = clientData.toString();
            byte[] buffer = str.getBytes(StandardCharsets.UTF_8);

            out.write(buffer);
            out.flush();
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    public static void editLastName(String lastName) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
            JSONObject clientData = new JSONObject(ClientTask.editLastName);

            byte[] buffer1 = lastName.getBytes();
            String str1 = new String(buffer1);
            clientData.remove("lastName");
            clientData.put("lastName", str1);

            String str = clientData.toString();
            byte[] buffer = str.getBytes(StandardCharsets.UTF_8);

            out.write(buffer);
            out.flush();
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    public static void editPhoneNumber(String phoneNumber) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
            JSONObject clientData = new JSONObject(ClientTask.editPhoneNumber);

            byte[] buffer1 = phoneNumber.getBytes();
            String str1 = new String(buffer1);
            clientData.remove("phoneNumber");
            clientData.put("phoneNumber", str1);

            String str = clientData.toString();
            byte[] buffer = str.getBytes(StandardCharsets.UTF_8);

            out.write(buffer);
            out.flush();
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    public static void editEmailAddress(String emailAddress) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
            JSONObject clientData = new JSONObject(ClientTask.editEmailAddress);

            byte[] buffer1 = emailAddress.getBytes();
            String str1 = new String(buffer1);
            clientData.remove("emailAddress");
            clientData.put("emailAddress", str1);

            String str = clientData.toString();
            byte[] buffer = str.getBytes(StandardCharsets.UTF_8);

            out.write(buffer);
            out.flush();
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    public static void editCity(String city) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
            JSONObject clientData = new JSONObject(ClientTask.editCity);

            byte[] buffer1 = city.getBytes();
            String str1 = new String(buffer1);
            clientData.remove("city");
            clientData.put("city", str1);

            String str = clientData.toString();
            byte[] buffer = str.getBytes(StandardCharsets.UTF_8);

            out.write(buffer);
            out.flush();
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    public static void editPassword(String password) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
            JSONObject clientData = new JSONObject(ClientTask.editPassword);

            byte[] buffer1 = password.getBytes();
            String str1 = new String(buffer1);
            clientData.remove("password");
            clientData.put("password", str1);

            String str = clientData.toString();
            byte[] buffer = str.getBytes(StandardCharsets.UTF_8);

            out.write(buffer);
            out.flush();
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    public static void chat(String receiverUsername, String message) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
            JSONObject clientData = new JSONObject(ClientTask.chat);

            byte[] buffer1 = receiverUsername.getBytes();
            byte[] buffer2 = message.getBytes();

            String str1 = new String(buffer1);
            clientData.remove("recipientId");
            clientData.put("recipientId", str1);

            String str2 = new String(buffer2);
            clientData.remove("message");
            clientData.put("message", str2);

            String str = clientData.toString();
            byte[] buffer = str.getBytes(StandardCharsets.UTF_8);
            out.write(buffer);
            out.flush();
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void loadProfile() throws IOException
    {
        try
        {
            out = socket.getOutputStream();
            JSONObject clientData = new JSONObject(ClientTask.loadProfile);
            String str = clientData.toString();
            byte[] buffer = str.getBytes(StandardCharsets.UTF_8);
            out.write(buffer);
            out.flush();
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void loadSearchResult(String group, String keyWord) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        JSONObject clientData = new JSONObject(ClientTask.extractSearchResult);
        clientData.remove("group");
        clientData.put("group", group);
        clientData.remove("keyWord");
        clientData.put("keyWord", keyWord);
        String str = clientData.toString();
        byte[] buffer = str.getBytes(StandardCharsets.UTF_8);
        out.write(buffer);
        out.flush();
    }

    public static void loadBookmarks() throws IOException
    {
        try
        {
            out = socket.getOutputStream();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        JSONObject clientData = new JSONObject(ClientTask.extractBookmarks);
        String str = clientData.toString();
        byte[] buffer = str.getBytes(StandardCharsets.UTF_8);
        out.write(buffer);
        out.flush();
    }

    public static void bookMark(String productName, String city, String category, String brand,
                                String price, String description, String username) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        JSONObject clientData = new JSONObject(ClientTask.bookmark);

        byte[] buffer1 = productName.getBytes();
        byte[] buffer2 = city.getBytes();
        byte[] buffer3 = category.getBytes();
        byte[] buffer4 = brand.getBytes();
        byte[] buffer5 = price.getBytes();
        byte[] buffer6 = description.getBytes();
        byte[] buffer8 = username.getBytes();

        String str1 = new String(buffer1);
        clientData.remove("name");
        clientData.put("name", str1);
        String str2 = new String(buffer2);
        clientData.remove("city");
        clientData.put("city", str2);
        String str3 = new String(buffer3);
        clientData.remove("category");
        clientData.put("category", str3);
        String str4 = new String(buffer4);
        clientData.remove("brand");
        clientData.put("brand", str4);
        String str5 = new String(buffer5);
        clientData.remove("price");
        clientData.put("price", str5);
        String str6 = new String(buffer6);
        clientData.remove("description");
        clientData.put("description", str6);
        String str8 = new String(buffer8);
        clientData.remove("username");
        clientData.put("username", str8);
        String str = clientData.toString();
        byte[] buffer = str.getBytes(StandardCharsets.UTF_8);
        out.write(buffer);
        out.flush();

    }


    public static void deleteBookMark(String productName, String city, String category, String brand,
                                String price, String description, String username) throws IOException
    {
        try
        {
            out = socket.getOutputStream();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        JSONObject clientData = new JSONObject(ClientTask.deleteBookmark);

        byte[] buffer1 = productName.getBytes();
        byte[] buffer2 = city.getBytes();
        byte[] buffer3 = category.getBytes();
        byte[] buffer4 = brand.getBytes();
        byte[] buffer5 = price.getBytes();
        byte[] buffer6 = description.getBytes();
        byte[] buffer8 = username.getBytes();

        String str1 = new String(buffer1);
        clientData.remove("name");
        clientData.put("name", str1);
        String str2 = new String(buffer2);
        clientData.remove("city");
        clientData.put("city", str2);
        String str3 = new String(buffer3);
        clientData.remove("category");
        clientData.put("category", str3);
        String str4 = new String(buffer4);
        clientData.remove("brand");
        clientData.put("brand", str4);
        String str5 = new String(buffer5);
        clientData.remove("price");
        clientData.put("price", str5);
        String str6 = new String(buffer6);
        clientData.remove("description");
        clientData.put("description", str6);
        String str8 = new String(buffer8);
        clientData.remove("username");
        clientData.put("username", str8);
        String str = clientData.toString();
        byte[] buffer = str.getBytes(StandardCharsets.UTF_8);
        out.write(buffer);
        out.flush();

    }

}