package models.client;

public interface ClickListener
{
    public void onClickListener(Advertisement advertisement);
}
