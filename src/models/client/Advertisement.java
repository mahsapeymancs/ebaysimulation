package models.client;

import javafx.scene.image.Image;
import org.json.JSONObject;

public class Advertisement
{
    private byte[] photo;
    private String username;
    private String category;
    private String city;
    private String name;
    private String brand;
    private String price;
    private String description;
    private String bookmark = "BOOKMARK_OUTLINE";
    private byte[] productImage;

    public void setUsername(String username) {this.username = username;}

    public void setCategory(String category)
    {
        this.category = category;
    }

    /*
        Default city for each advertisement that become public by one of our users, is user's city,
    which had been set while signing up.
    But there's an option that each user can add and ad for another city and change the default city.
     */

    public String getUsername() {
        return username;
    }

    public String getCategory() {
        return category;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public String getPrice()
    {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String addNewAds(Advertisement advertisement)
    {
        //Image is not handled here...
        JSONObject adsData = new JSONObject();
        adsData.put("task", "addNewAds");
        adsData.put("name", advertisement.getName());
        adsData.put("username", advertisement.getUsername());
        adsData.put("category", advertisement.getCategory());
        adsData.put("city", advertisement.getCity());
        adsData.put("brand", advertisement.getBrand());
        adsData.put("price", advertisement.getPrice());
        adsData.put("description", advertisement.getDescription());
        String data = adsData.toString();
        return data;
        //The string that this function returns, should be cast to byte[] array and sent to server.
        // byte[] buffer = data.getBytes(StandardCharsets.UTF_8);
        //                    out.write(buffer);
        //                    out.flush();
    }

    public String getBookmark() {
        return bookmark;
    }

    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
    }

    public byte[] getProductImage() {
        return productImage;
    }

    public void setProductImage(byte[] productImage) {
        this.productImage = productImage;
    }
}