package controllers;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class PagesController
{
    private static Stage stage;

    public static void closePage(ActionEvent actionEvent)
    {
        Stage newStage = (Stage)((Node) actionEvent.getSource()).getScene().getWindow();
        newStage.close();
    }

    public static void closePage()
    {
        stage.close();
    }

    public static void openPage(String name) throws IOException
    {
        stage = new Stage();
        Parent root = FXMLLoader.load(PagesController.class.getResource("../views/" + name + ".fxml"));

        stage.setScene(new Scene(root));

        stage.initStyle(StageStyle.TRANSPARENT);
        MouseController.handle(root, stage);

        stage.show();
    }

    public static void menuBar(JFXHamburger burger, JFXDrawer menuBar) throws IOException
    {
        VBox vbox = FXMLLoader.load(PagesController.class.getResource("../views/" + "menuBar.fxml"));
        menuBar.setSidePane(vbox);

        HamburgerBackArrowBasicTransition back = new HamburgerBackArrowBasicTransition(burger);
        back.setRate(-1);
        burger.addEventHandler(MouseEvent.MOUSE_CLICKED, (e)->
        {
            back.setRate(back.getRate() * (-1));
            back.play();

            if (menuBar.isShown())
            {
                menuBar.close();
            }
            else
            {
                menuBar.open();
            }
        });
    }
}