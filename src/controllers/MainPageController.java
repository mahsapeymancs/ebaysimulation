package controllers;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import models.Main;
import models.client.Advertisement;
import models.client.ClickListener;
import models.client.Client;
import models.client.ClientListener;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MainPageController implements Initializable
{
    @FXML
    private StackPane stack;

    @FXML
    private Label productName;

    @FXML
    private Label productCost;

    @FXML
    private Label labelSearch;

    @FXML
    private JFXHamburger burger;

    @FXML
    private GridPane grid;

    @FXML
    private ImageView animation;

    @FXML
    private JFXDrawer menuBar;

    @FXML
    private ImageView productImage;

    @FXML
    private MaterialDesignIconView bookmark;

    private ClickListener listener;
    private ArrayList<String> ads = new ArrayList<>();
    private List<Advertisement> advertisements = new ArrayList<>();

    private List<Advertisement> getData()
    {
        List<Advertisement> data = new ArrayList<>();
        Advertisement advertisement;

        for (int i = 0; i < ClientListener.homeAdvertisements.size(); i++)
        {
            advertisement = new Advertisement();
            advertisement.setName(ClientListener.homeAdvertisements.get(i).getName());
            advertisement.setPrice(ClientListener.homeAdvertisements.get(i).getPrice());
            advertisement.setBrand(ClientListener.homeAdvertisements.get(i).getBrand());
            advertisement.setDescription(ClientListener.homeAdvertisements.get(i).getDescription());
            advertisement.setUsername(ClientListener.homeAdvertisements.get(i).getUsername());
            advertisement.setCity(ClientListener.homeAdvertisements.get(i).getCity());
            advertisement.setCategory(ClientListener.homeAdvertisements.get(i).getCategory());
            advertisement.setBookmark(ClientListener.homeAdvertisements.get(i).getBookmark());
            System.out.println("aliii " + advertisement.getBookmark());
            data.add(advertisement);
        }

        return data;
    }

    private void setChosen(Advertisement advertisement) throws IOException
    {
        productName.setText(advertisement.getName());
        productCost.setText(Main.CURRENCY + advertisement.getPrice());
        bookmark.setGlyphName(Main.currentAdvertisement.getBookmark());
        BufferedImage bImage = ImageIO.read(new File("D:\\AdsImages" + "/" + Main.currentAdvertisement.getName() + ".jpg"));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bImage, "jpg", bos );
        byte [] data = bos.toByteArray();
        productImage.setImage(new Image(new ByteArrayInputStream(data)));
    }

    private static int counter = 4;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        ads = new ArrayList<>();
        ads.add("eBay-shop.png");
        ads.add("ebay.gif");
        ads.add("giphy12.gif");
        ads.add("shop.gif");
        ads.add("ebay-drible-dobre.gif");

        advertisements.addAll(getData());

        if (!advertisements.isEmpty())
        {
            Main.currentAdvertisement = advertisements.get(0);
            productName.setText(Main.currentAdvertisement.getName());
            productCost.setText(Main.CURRENCY + Main.currentAdvertisement.getPrice());
            bookmark.setGlyphName(Main.currentAdvertisement.getBookmark());
            System.out.println("aliii3 " + Main.currentAdvertisement.getBookmark());
            BufferedImage bImage = null;
            try
            {
                bImage = ImageIO.read(new File("D:\\AdsImages" + "/" + Main.currentAdvertisement.getName() + ".jpg"));
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try
            {
                ImageIO.write(bImage, "jpg", bos );
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
            byte [] data = bos.toByteArray();
            productImage.setImage(new Image(new ByteArrayInputStream(data)));
        }

        listener = new ClickListener()
        {
            @Override
            public void onClickListener(Advertisement advertisement)
            {
                try
                {
                    setChosen(advertisement);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        };
        int column = 1;
        int row = 1;
        try
        {
            PagesController.menuBar(burger, menuBar);
            if (!advertisements.isEmpty())
            {
                for (int i = 0; i < advertisements.size(); i++)
                {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(getClass().getResource("../views/advertisement.fxml"));
                    AnchorPane anchorPane = fxmlLoader.load();

                    AdvertisementController advertisementController = fxmlLoader.getController();
                    advertisementController.setData(advertisements.get(i), listener);

                    if (column == 4)
                    {
                        column = 1;
                        row++;
                    }

                    grid.add(anchorPane, column++, row);
                    GridPane.setMargin(anchorPane, new Insets(10));
                }
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void backAd(MouseEvent event)
    {
        counter--;
        if (counter < 0)
        {
            counter = 4;
        }

        File file = new File("src/views/pictures/" + ads.get(counter));
        Image newImage = new Image(file.toURI().toString());
        animation.setImage(newImage);
    }

    @FXML
    void nextAd(MouseEvent event)
    {
        counter++;
        if (counter > 4)
        {
            counter = 0;
        }

        File file = new File("src/views/pictures/" + ads.get(counter));
        Image newImage = new Image(file.toURI().toString());
        animation.setImage(newImage);
    }

    public void search(MouseEvent mouseEvent) throws IOException
    {
        labelSearch.setText(SearchController.getStep1() + " / " + SearchController.getStep2());
        Client.loadSearchResult(SearchController.getStep1(), SearchController.getStep2());
    }

    public void startChat(ActionEvent actionEvent) throws IOException
    {
        PagesController.closePage();
        PagesController.openPage("chat");
    }

    public void showInfo(ActionEvent actionEvent)
    {
        try
        {
            Parent root = FXMLLoader.load(getClass().getResource("../views/" + "information" + ".fxml"));
            JFXDialog dialog = new JFXDialog(stack, (Region) root, JFXDialog.DialogTransition.CENTER);
            dialog.show();

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void dialogSearch(MouseEvent event)
    {
        try
        {
            Parent root = FXMLLoader.load(getClass().getResource("../views/" + "searchTwoStep" + ".fxml"));
            JFXDialog dialog = new JFXDialog(stack, (Region) root, JFXDialog.DialogTransition.TOP);
            dialog.show();

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @FXML
    void changeBookMark(MouseEvent event) throws IOException
    {
        if (bookmark.getGlyphName().equals("BOOKMARK_OUTLINE"))
        {
            bookmark.setGlyphName("BOOKMARK");
            Main.currentAdvertisement.setBookmark(bookmark.getGlyphName());
            Client.bookMark(Main.currentAdvertisement.getName(), Main.currentAdvertisement.getCity(),
                    Main.currentAdvertisement.getCategory(),Main.currentAdvertisement.getBrand(),
                    Main.currentAdvertisement.getPrice(), Main.currentAdvertisement.getDescription(),
                    Main.currentAdvertisement.getUsername());
            System.out.println("aliii2 " + Main.currentAdvertisement.getBookmark());
        }
        else
        {
            bookmark.setGlyphName("BOOKMARK_OUTLINE");
            Main.currentAdvertisement.setBookmark(bookmark.getGlyphName());
            Client.deleteBookMark(Main.currentAdvertisement.getName(), Main.currentAdvertisement.getCity(),
                    Main.currentAdvertisement.getCategory(), Main.currentAdvertisement.getBrand(),
                    Main.currentAdvertisement.getPrice(), Main.currentAdvertisement.getDescription(),
                    Main.currentAdvertisement.getUsername());

        }

    }
}
