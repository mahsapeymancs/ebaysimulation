package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import models.Main;
import models.client.Client;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MenuBarController
{
    @FXML
    void openHelp(MouseEvent event) throws IOException
    {
        PagesController.closePage();
        PagesController.openPage("help");
    }

    @FXML
    void openHome(MouseEvent event) throws IOException
    {
        PagesController.closePage();
        PagesController.openPage("mainPage");
    }

    @FXML
    void openLogOut(MouseEvent event)
    {
        PagesController.closePage();
    }

    @FXML
    void openProfile(MouseEvent event) throws IOException
    {
        Client.loadProfile();
        PagesController.closePage();
        PagesController.openPage("personalAccount");
    }

    @FXML
    void openSaved(MouseEvent event) throws IOException
    {
        Client.loadBookmarks();
        PagesController.closePage();
        PagesController.openPage("savedProducts");
    }

    @FXML
    void openAdding(MouseEvent event) throws IOException
    {
        PagesController.closePage();
        PagesController.openPage("addAdvertisement");
    }
}