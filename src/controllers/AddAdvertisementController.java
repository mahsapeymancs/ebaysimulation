package controllers;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import models.client.Client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ResourceBundle;

public class AddAdvertisementController implements Initializable
{
    @FXML
    private JFXHamburger burger;

    @FXML
    private Circle imageProduct;

    @FXML
    private JFXTextField name;

    @FXML
    private JFXTextField price;

    @FXML
    private JFXTextField brand;

    @FXML
    private JFXTextArea description;

    @FXML
    private JFXComboBox<String> listCities;

    @FXML
    private JFXComboBox<String> listCategory;

    @FXML
    private JFXDrawer menuBar;

    private FileChooser fileChooser;

    private byte[] photo;

    @FXML
    void finish(MouseEvent event) throws IOException
    {
        Client.addNewAds(name.getText(), listCities.getValue().toString(), listCategory.getValue().toString()
                ,brand.getText(), price.getText(), description.getText(), photo);

    }

    @FXML
    void showEmail(MouseEvent event)
    {

    }

    @FXML
    void showPhone(MouseEvent event)
    {

    }

    @FXML
    void showPhoto(MouseEvent event)
    {

    }


    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        try
        {
            PagesController.menuBar(burger, menuBar);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png", "*.jpeg");
        fileChooser.getExtensionFilters().add(filter);

        fillComboCity();
        fillComboCategory();

    }

    @FXML
    void choosePhoto(MouseEvent event) throws IOException
    {
        try
        {
            File file = fileChooser.showOpenDialog(null);
            photo = Files.readAllBytes(file.toPath());
            Image img = new Image(new ByteArrayInputStream(photo));
            imageProduct.setFill(new ImagePattern(img));
        }
        catch (Exception e)
        {

        }
    }


    public void fillComboCity()
    {
        listCities.getItems().addAll("London", "Liverpool", "Manchester", "Cambridge",
                "Oxford", "Winchester", "Sunderland", "Lincoln");
    }

    public void fillComboCategory()
    {
        listCategory.getItems().addAll("Clothes", "Books", "Art", "Science",
                "Accessories", "Boots", "Cosmetics", "Carpet");
    }

}
