package controllers;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTextField;
import controllers.PagesController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ChatWithCustomerController implements Initializable {

    @FXML
    private VBox chosenProduct;

    @FXML
    private Label productName;

    @FXML
    private ImageView productImage;

    @FXML
    private Label productCost;

    @FXML
    private Circle imageAudience;

    @FXML
    private Label customerUserName;

    @FXML
    private VBox showChats;

    @FXML
    private JFXTextField message;

    @FXML
    private JFXHamburger burger;

    @FXML
    private JFXDrawer menuBar;

    @FXML
    void sendMessage(ActionEvent event) {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        try
        {
            PagesController.menuBar(burger, menuBar);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

}
