package controllers;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import models.client.Client;
import models.client.ClientListener;

import java.io.IOException;

public class LoginController
{

    @FXML
    private JFXTextField username;

    @FXML
    private JFXPasswordField password;

    @FXML
    private FontAwesomeIconView visibility;

    @FXML
    private Label alert;

    @FXML
    void creation(ActionEvent event) throws IOException
    {
        PagesController.closePage();
        PagesController.openPage("signup");
    }

    @FXML
    void exit(MouseEvent event)
    {
        PagesController.closePage();
    }

    @FXML
    public void login() throws IOException
    {
        Client.login(username.getText(), password.getText());
    }


    @FXML
    void visibility(MouseEvent event)
    {
        if (visibility.getGlyphName().equals("EYE_SLASH"))
        {
            visibility.setGlyphName("EYE");
        }
        else
        {
            visibility.setGlyphName("EYE_SLASH");
        }
    }

    public Label getAlert() {
        return alert;
    }

    public void setAlert(Label alert)
    {
        this.alert = alert;
    }
}
