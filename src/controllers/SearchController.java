package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ToggleGroup;

public class SearchController
{
    @FXML
    private ToggleGroup twoStep;

    @FXML
    private ToggleGroup chooseOne;

    private static String step1;
    private static String step2;

    @FXML
    void category(ActionEvent event)
    {
        step1 = "category";
    }

    @FXML
    void city(ActionEvent event)
    {
        step1 = "city";
    }

    @FXML
    void accessories(ActionEvent event)
    {
        step2 = "accessories";
    }

    @FXML
    void art(ActionEvent event)
    {
        step2 = "art";
    }

    @FXML
    void books(ActionEvent event)
    {
        step2 = "books";
    }

    @FXML
    void boots(ActionEvent event)
    {
        step2 = "boots";
    }

    @FXML
    void carpet(ActionEvent event)
    {
        step2 = "carpet";
    }

    @FXML
    void clothes(ActionEvent event)
    {
        step2 = "clothes";
    }


    @FXML
    void science(ActionEvent event)
    {
        step2 = "science";
    }

    @FXML
    void cosmetics(ActionEvent event)
    {
        step2 = "cosmetics";
    }

    @FXML
    void lincoln(ActionEvent event)
    {
        step2 = "lincoln";
    }

    @FXML
    void liverpool(ActionEvent event)
    {
        step2 = "liverpool";
    }

    @FXML
    void london(ActionEvent event)
    {
        step2 = "london";
    }

    @FXML
    void manchester(ActionEvent event)
    {
        step2 = "manchester";
    }

    @FXML
    void oxford(ActionEvent event)
    {
        step2 = "oxford";
    }

    @FXML
    void sunderland(ActionEvent event)
    {
        step2 = "sunderland";
    }

    @FXML
    void winchester(ActionEvent event)
    {
        step2 = "winchester";
    }

    @FXML
    void cambridge(ActionEvent event)
    {
        step2 = "cambridge";
    }

    public static String getStep1()
    {
        return step1;
    }

    public static String getStep2()
    {
        return step2;
    }
}
