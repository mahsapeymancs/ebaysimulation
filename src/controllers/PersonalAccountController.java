package controllers;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import models.Main;
import models.client.Client;
import models.client.ClientListener;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.util.ResourceBundle;

public class PersonalAccountController implements Initializable
{
    private FileChooser fileChooser;

    private byte[] photo;

    @FXML
    private JFXHamburger burger;

    @FXML
    private JFXTextField lastName;

    @FXML
    private JFXTextField firstName;

    @FXML
    private Circle imageUser;

    @FXML
    private JFXTextField email;

    @FXML
    private JFXTextField phone;

    @FXML
    private JFXComboBox<String> listCities;

    @FXML
    private JFXDrawer menuBar;

    @FXML
    private Label userName;

    @FXML
    private JFXButton editFirstName;

    @FXML
    private JFXButton editLastName;

    @FXML
    private JFXButton editCity;

    @FXML
    private JFXButton editPhone;

    @FXML
    private JFXButton editEmail;

    @FXML
    private JFXPasswordField pastPass;

    @FXML
    private JFXPasswordField newPass;

    @FXML
    private JFXButton pastPassBtn;

    @FXML
    private JFXButton newPassBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        userName.setText(Main.currentUser.getId());
        firstName.setText(Main.currentUser.getFirstName());
        lastName.setText(Main.currentUser.getLastName());
        BufferedImage bImage2 = null;
        try {
            bImage2 = ImageIO.read(new File("D:\\ProfileImages" + "/" + Main.currentUser.getEmailAddress() + ".jpg"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
        try {
            ImageIO.write(bImage2, "jpg", bos2);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        byte [] data2 = bos2.toByteArray();
        Image img = new Image(new ByteArrayInputStream(data2));
        imageUser.setFill(new ImagePattern(img));

//        imageUser.setImage(ClientListener.currentUser.getImage());
        email.setText(Main.currentUser.getEmailAddress());
        phone.setText(Main.currentUser.getPhoneNumber());

        try
        {
            PagesController.menuBar(burger, menuBar);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
        fileChooser.getExtensionFilters().add(filter);

        fillCombo();
    }

    @FXML
    void choosePhoto(MouseEvent event)
    {
        try
        {
            File file = fileChooser.showOpenDialog(null);
            photo = Files.readAllBytes(file.toPath());
            Image img = new Image(new ByteArrayInputStream(photo));
            imageUser.setFill(new ImagePattern(img));
        }
        catch (Exception e)
        {

        }
    }

    @FXML
    void checkPastBtn(MouseEvent event)
    {

    }

    @FXML
    void editCity(MouseEvent event) throws IOException
    {
        Client.editCity(listCities.getValue().toString());
    }

    @FXML
    void editEmail(MouseEvent event) throws IOException
    {
        Client.editEmailAddress(email.getText());
    }

    @FXML
    void editFname(MouseEvent event) throws IOException
    {
        Client.editFirstName(firstName.getText());
    }

    @FXML
    void editImage(MouseEvent event) throws IOException
    {
        FileOutputStream file = new FileOutputStream("D:\\ProfileImages" + "/" + userName.getText() + ".jpg");
        file.write(photo, 0, photo.length);
    }

    @FXML
    void editLname(MouseEvent event) throws IOException
    {
        Client.editLastName(lastName.getText());
    }

    @FXML
    void editPassword(MouseEvent event) throws IOException
    {
        Client.editPassword(newPass.getText());
    }

    @FXML
    void editPhone(MouseEvent event) throws IOException
    {
        Client.editPhoneNumber(phone.getText());
    }

    public void fillCombo()
    {
        listCities.getItems().addAll("London", "Liverpool", "Manchester", "Cambridge",
                "Oxford", "Winchester", "Sunderland", "Lincoln");
    }
}