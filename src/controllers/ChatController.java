package controllers;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import models.Main;
import models.client.Client;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ChatController implements Initializable
{
    @FXML
    private VBox chosenProduct;

    @FXML
    private Label productName;

    @FXML
    private ImageView productImage;

    @FXML
    private Label productCost;

    @FXML
    private Label username;

    @FXML
    private Label brand;

    @FXML
    private Label category;

    @FXML
    private Label city;

    @FXML
    private Label description;

    @FXML
    private Label yourEmail;

    @FXML
    private Label yourPhone;

    @FXML
    private Circle yourImage;

    @FXML
    private Label otherUsername;

    @FXML
    private Circle imageAudience;

    @FXML
    private VBox showChats;

    @FXML
    private JFXDrawer menuBar;

    @FXML
    private JFXHamburger burger;

    @FXML
    private JFXTextField message;

    @FXML
    void sendMessage(ActionEvent event) throws IOException
    {
        if ((message.getText() != null && !message.getText().isEmpty()))
        {
            Label newMessage = new Label(message.getText());
            Label empty = new Label("\n");
            newMessage.setFont(Font.font ("Cambria", FontWeight.BOLD, 16));
            newMessage.setStyle("-fx-background-color: #b4e1fb; -fx-background-radius: 20; -fx-border-width: inherit; -fx-label-padding: 10");
            Client.chat(otherUsername.getText(), message.getText());
            message.clear();
            showChats.getChildren().addAll(newMessage, empty);

        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        try
        {
            PagesController.menuBar(burger, menuBar);
            otherUsername.setText(Main.currentAdvertisement.getUsername());
            BufferedImage bImage2 = ImageIO.read(new File("D:\\ProfileImages" + "/" + Main.currentAdvertisement.getUsername() + ".jpg"));
            ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
            ImageIO.write(bImage2, "jpg", bos2);
            byte [] data2 = bos2.toByteArray();
            Image img = new Image(new ByteArrayInputStream(data2));
            imageAudience.setFill(new ImagePattern(img));
            productName.setText(Main.currentAdvertisement.getName());
            productCost.setText(Main.currentAdvertisement.getPrice());
            BufferedImage bImage = ImageIO.read(new File("D:\\AdsImages" + "/" + Main.currentAdvertisement.getName() + ".jpg"));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(bImage, "jpg", bos );
            byte [] data = bos.toByteArray();
            productImage.setImage(new Image(new ByteArrayInputStream(data)));
            username.setText(Main.currentAdvertisement.getUsername());
            description.setText(Main.currentAdvertisement.getDescription());
            brand.setText(Main.currentAdvertisement.getBrand());
            city.setText(Main.currentAdvertisement.getCity());
            category.setText(Main.currentAdvertisement.getCategory());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}