package controllers;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import models.client.Client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ResourceBundle;

import javafx.stage.FileChooser;

public class SignupController implements Initializable
{
    private FileChooser fileChooser;

    private byte[] photo;

    @FXML
    private JFXTextField lastName;

    @FXML
    private JFXTextField firstName;

    @FXML
    private Circle imageUser;

    @FXML
    private ComboBox listCities;

    @FXML
    private JFXTextField email;

    @FXML
    private JFXTextField phone;

    @FXML
    private JFXPasswordField password;
    @FXML
    private Text choose;

    @FXML
    void exit(MouseEvent event)
    {
        PagesController.closePage();
    }

    @FXML
    void openSignin(MouseEvent event) throws IOException
    {
        PagesController.closePage();
        PagesController.openPage("login");
    }

    @FXML
    void signup(MouseEvent event) throws IOException
    {
        Client.signup(firstName.getText(), lastName.getText(), listCities.getValue().toString(), email.getText(), phone.getText(), password.getText(), photo);
    }

    @FXML
    void choosePhoto(MouseEvent event) throws IOException
    {
        try
        {
            File file = fileChooser.showOpenDialog(null);
            photo = Files.readAllBytes(file.toPath());
            Image img = new Image(new ByteArrayInputStream(photo));
            imageUser.setFill(new ImagePattern(img));
        }
        catch (Exception e)
        {

        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
        fileChooser.getExtensionFilters().add(filter);

        fillCombo();
    }

    public void fillCombo()
    {
        listCities.getItems().addAll("London", "Liverpool", "Manchester", "Cambridge",
                                            "Oxford", "Winchester", "Sunderland", "Lincoln");
    }
}
