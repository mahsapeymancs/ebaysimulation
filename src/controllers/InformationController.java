package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import models.Main;
import models.client.Advertisement;
import models.client.ClickListener;

import java.net.URL;
import java.util.ResourceBundle;

public class InformationController implements Initializable
{
    @FXML
    private Label username;

    @FXML
    private Label brand;

    @FXML
    private Label category;

    @FXML
    private Label city;

    @FXML
    private Label description;


    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        username.setText("Username: " + Main.currentAdvertisement.getUsername());
        brand.setText("Brand: " + Main.currentAdvertisement.getBrand());
        category.setText("Category: " + Main.currentAdvertisement.getCategory());
        city.setText("City: " + Main.currentAdvertisement.getCity());
        description.setText("Description: " + Main.currentAdvertisement.getDescription());
    }
}
