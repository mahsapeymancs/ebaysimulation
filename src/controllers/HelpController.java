package controllers;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HelpController implements Initializable
{
    @FXML
    private JFXDrawer menuBar;

    @FXML
    private JFXHamburger burger;
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        try
        {
            PagesController.menuBar(burger, menuBar);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
