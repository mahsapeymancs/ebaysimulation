package controllers;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import models.Main;
import models.client.Advertisement;
import models.client.ClickListener;
import models.client.Client;
import models.client.ClientListener;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class SavedProductsController implements Initializable
{
    @FXML
    private StackPane stack;

    @FXML
    private Label productName;

    @FXML
    private Label productCost;

    @FXML
    private JFXHamburger burger;

    @FXML
    private GridPane grid;

    @FXML
    private JFXDrawer menuBar;

    @FXML
    private ImageView productImage;

    @FXML
    private MaterialDesignIconView bookmark;

    private Image image;

    private ClickListener listener;

    private List<Advertisement> advertisements = new ArrayList<>();

    private List<Advertisement> getData()
    {
        List<Advertisement> data = new ArrayList<>();
        Advertisement advertisement;

        for (int i = 0; i < ClientListener.bookmarkedAds.size(); i++)
        {
            advertisement = new Advertisement();
            advertisement.setName(ClientListener.bookmarkedAds.get(i).getName());
            advertisement.setPrice(ClientListener.bookmarkedAds.get(i).getPrice());
            advertisement.setBrand(ClientListener.bookmarkedAds.get(i).getBrand());
            advertisement.setDescription(ClientListener.bookmarkedAds.get(i).getDescription());
            advertisement.setUsername(ClientListener.bookmarkedAds.get(i).getUsername());
            advertisement.setCity(ClientListener.bookmarkedAds.get(i).getCity());
            advertisement.setCategory(ClientListener.bookmarkedAds.get(i).getCategory());
            advertisement.setBookmark(ClientListener.bookmarkedAds.get(i).getBookmark());
            data.add(advertisement);
        }

        return data;
    }

    private void setChosen(Advertisement advertisement)
    {
        productName.setText(advertisement.getName());
        productCost.setText(Main.CURRENCY + advertisement.getPrice());
        bookmark.setGlyphName(Main.currentAdvertisement.getBookmark());
        BufferedImage bImage = null;
        try
        {
            bImage = ImageIO.read(new File("D:\\AdsImages" + "/" + Main.currentAdvertisement.getName() + ".jpg"));
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try
        {
            ImageIO.write(bImage, "jpg", bos );
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        byte [] data = bos.toByteArray();
        productImage.setImage(new Image(new ByteArrayInputStream(data)));
    }
    private static int counter = 4;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
//        advertisements = ClientListener.getAdvertisements();

        advertisements.addAll(getData());

        if (!advertisements.isEmpty())
        {
            Main.currentAdvertisement = advertisements.get(0);
            productName.setText(Main.currentAdvertisement.getName());
            productCost.setText(Main.CURRENCY + Main.currentAdvertisement.getPrice());
            bookmark.setGlyphName(Main.currentAdvertisement.getBookmark());

            BufferedImage bImage = null;
            try {
                bImage = ImageIO.read(new File("D:\\AdsImages" + "/" + Main.currentAdvertisement.getName() + ".jpg"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                ImageIO.write(bImage, "jpg", bos );
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            byte [] data = bos.toByteArray();
            productImage.setImage(new Image(new ByteArrayInputStream(data)));
        }

        listener = new ClickListener()
        {
            @Override
            public void onClickListener(Advertisement advertisement)
            {
                setChosen(advertisement);
            }
        };

        int column = 1;
        int row = 1;

        try
        {
            PagesController.menuBar(burger, menuBar);
            for (int i = 0; i < advertisements.size(); i++)
            {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("../views/bookmarkAdvertisement.fxml"));
                AnchorPane anchorPane = fxmlLoader.load();
                AdvertisementController advertisementController = fxmlLoader.getController();

                advertisementController.setData(advertisements.get(i), listener);

                if (column == 4)
                {
                    column = 1;
                    row++;
                }

                grid.add(anchorPane, column++, row);
                GridPane.setMargin(anchorPane, new Insets(10));
            }
        }

        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void startChat(ActionEvent actionEvent) throws IOException
    {
        PagesController.closePage();
        PagesController.openPage("chat");
    }

    public void showInfo(ActionEvent actionEvent)
    {
        try
        {
            Parent root = FXMLLoader.load(PagesController.class.getResource("../views/information.fxml"));
            JFXDialog dialog = new JFXDialog(stack, (Region) root, JFXDialog.DialogTransition.CENTER);
            dialog.show();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

    }


    @FXML
    void changeBookMark(MouseEvent event) throws IOException
    {
        if (bookmark.getGlyphName().equals("BOOKMARK_OUTLINE"))
        {
            bookmark.setGlyphName("BOOKMARK");
            Main.currentAdvertisement.setBookmark(bookmark.getGlyphName());
            Client.bookMark(Main.currentAdvertisement.getName(), Main.currentAdvertisement.getCity(),
                    Main.currentAdvertisement.getCategory(),Main.currentAdvertisement.getBrand(),
                    Main.currentAdvertisement.getPrice(), Main.currentAdvertisement.getDescription(),
                    Main.currentAdvertisement.getUsername());

        }
        else
        {
            bookmark.setGlyphName("BOOKMARK_OUTLINE");
            Main.currentAdvertisement.setBookmark(bookmark.getGlyphName());
            Client.deleteBookMark(Main.currentAdvertisement.getName(), Main.currentAdvertisement.getCity(),
                    Main.currentAdvertisement.getCategory(), Main.currentAdvertisement.getBrand(),
                    Main.currentAdvertisement.getPrice(), Main.currentAdvertisement.getDescription(),
                    Main.currentAdvertisement.getUsername());

        }

    }
}
