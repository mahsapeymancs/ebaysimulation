package controllers;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import models.Main;
import models.client.Advertisement;
import models.client.ClickListener;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class AdvertisementController
{
    @FXML
    private Label nameLabel;

    @FXML
    private Label costLabel;

    @FXML
    private ImageView imageProduct;

    @FXML
    public MaterialDesignIconView bookmark;

    private Advertisement advertisement;
    private ClickListener listener;

    @FXML
    void click(MouseEvent event)
    {
        Main.currentAdvertisement = advertisement;

        listener.onClickListener(advertisement);
    }

    public void setData(Advertisement advertisement, ClickListener listener) throws IOException
    {
        this.advertisement = advertisement;
        this.listener = listener;
        nameLabel.setText(advertisement.getName());
        costLabel.setText(Main.CURRENCY + advertisement.getPrice());
        System.out.println(advertisement.getBookmark().isEmpty());
//        bookmark.setGlyphName(advertisement.getBookmark());
        BufferedImage bImage2 = ImageIO.read(new File("D:\\AdsImages" + "/" + nameLabel.getText() + ".jpg"));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bImage2, "jpg", bos );
        byte [] data = bos.toByteArray();
        imageProduct.setImage(new Image(new ByteArrayInputStream(data)));

    }

    public void setBookmark(MaterialDesignIconView bookmark)
    {
        this.bookmark = bookmark;
    }
}
